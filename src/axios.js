import axios from "axios";

export const axiosAPI = axios.create({
  baseURL: `${process.env.VUE_APP_API}`
});
