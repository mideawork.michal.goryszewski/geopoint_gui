import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

/* eslint-disable no-new */
const store = new Vuex.Store({
  state: {
    points: []
  },
  mutations: {
    addPoint(state) {
      let count = state.points.length;
      let payload = { id: count, longitude: 0, latitude: 0 };
      state.points.push(payload);
    },
    removePoint(state) {
      state.points.pop();
    },
    updatePoint(state, payload) {
      let id = state.points.findIndex(el => payload.id === el.id);
      console.log(state.points);
      state.points[id][payload.name] = payload.value;
      state.points.push({});
      state.points.pop();
    },
    setPoint(state, payload) {
      let id = state.points.findIndex(el => payload.id === el.id);
      state.points[id] = payload;

      state.points.push({});
      state.points.pop();
    },
    clearPoint(state) {
      state.points = [];
    }
  },
  getters: {
    getCountPoint: state => {
      return state.points.length;
    },
    getPoints: state => {
      return state.points;
    }
  },
  actions: {
    actionAddPoint({ commit }) {
      commit("addPoint");
    },
    actionRemovePoint({ commit }) {
      commit("removePoint");
    },
    actionUpdatePoint({ commit }, payload) {
      console.log(payload);
      commit("updatePoint", payload);
    },

    actionSetPoint({ commit, getters }, payload) {
      let count = getters.getPoints.length;
      if (count <= payload.id) {
        commit("addPoint");
      }

      commit("setPoint", payload);
    },
    actionClearPoint({ commit }) {
      commit("clearPoint");
    }
  }
});

export default store;
