/* eslint-disable */
import Vue from "vue";
import App from "./App.vue";
import dotenv from "dotenv";
import vuetify from "./plugins/vuetify";
import Vuelidate from "vuelidate";
import store from "./store";
Vue.use(Vuelidate);
dotenv.config();
Vue.config.productionTip = false;

new Vue({
  vuetify,
  store,
  render: h => h(App)
}).$mount("#app");
